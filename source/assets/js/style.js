$(document).ready(function(){

	$('.head-items').find('li').click(function(){
		$('li').removeClass('active');
		$(this).addClass('active');
	});

	$(document).scroll(function(){
		var header = $(".header");
		if($(window).scrollTop() > 0){
			header.addClass( "on-scroll" );
		}
		else{
			header.removeClass( "on-scroll" );
		}
	});


	var button = $(".button");
	button.click(function( e ) {
		e.preventDefault();
		var head_button = $( this ).closest('.header-banner'),
			title, content ;
		if( head_button.length > 0){
			title = head_button.find('.active').find('h1').text();
			content = head_button.find('.active').find('p').text();
		} else {
			alert( 'khong co noi dung ');return;
		}
		popup(title, content);
	});	

	$('.header-banner').owlCarousel({
	    loop:true,
	    items:1,
	    nav:false,
	    autoplay:true
	})

	$('.head-items').click(function( e ){
		e.preventDefault();
		if( $('.log-in').hasClass('active') && $('.login').length <= 0 ){
			$.ajax({
		        url: 'lognin.html',
		        type: 'POST',
		        dataType: 'html',
		        success: function( data ) {
		        	$('header').append( data );
			    }
		    })
		}
		else{
			$('.login').remove();
		}
	})

	$('.head-items').click(function( e ){
		e.preventDefault();
		if( $('.sign-up').hasClass('active') && $('.signup').length <= 0 ){
			$.ajax({
		        url: 'signup.html',
		        type: 'POST',
		        dataType: 'html',
		        success: function( data ) {
		        	$('header').append( data );
			    }
		    })
		}
		else {
			$('.signup').remove();
		}
	})

	$('.head-items').click(function( e ){
		e.preventDefault();
		if( $('.explore').hasClass('active') && $('.blocks').length <= 0 ){
			$('.main-home-page').addClass("false");
			$.ajax({
		        url: 'blocks.html',
		        type: 'POST',
		        dataType: 'html',
		        success: function( data ) {
		        	$('.primary-content').append( data );
			    }
		    })
		}
		else {
			$('.main-home-page').removeClass("false");
			$('.blocks').remove();
		}
	})
	$('.products').owlCarousel({
	    loop:false,
	    items:3,
	    nav:true,
	    margin:15,
	})
	
	$('.tourists_attraction').owlCarousel({
	    loop:false,
	    items:5,
	    nav:false,
	})

});