(function($){
	if(typeof THEME == "undefined" ){
		THEME = {};
		THEME.articles = false;
	}
	
	THEME.get_articles = function(){
		$.ajax({
	        url: 'assets/json/data.json',
	        type: 'POST',
	        dataType: 'json',
	        success: function( data ) {
	        	THEME.articles = data;
	        	THEME.load_trending();
		    }
	    })
	};//THEME.get_articles

	THEME.load_trending = function ( ) {
		const item = THEME.articles,
			  display = 6 ,
			  blogs = $('.list-blogs');
		for( var i = 0; i < display ; i++ ){
    		$('<article>', {
				'class': 'blog-item',
			})
			.append(
				$('<picture>', {
					'class':'blog-item-thumbnail'
				})
				.append(
					$('<a>',{
						'href':item[0].link
					})
					.append(
						$('<img>',{
							'src': "images/" + item[0].picture
						})
					)
				)
			)
			.append(
				$('<div>',{
					'class':'blog-item-content'
				})
				.append(
					$('<h3>',{
						'class':'blog-item-name'
					})
					.append(
						$('<a>',{
							'href':item[0].link,
							text : item[0].title
						})
					)
				)
				.append(
					$('<p>',{
						'class':'blog-item-summary',
						text: item[0].content
					})
				)
			).appendTo( blogs );
    	}
	}//THEME.load_trending


	$(document).ready(function(){
		THEME.get_articles();
	});
})(jQuery);